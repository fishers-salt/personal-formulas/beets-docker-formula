# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as beets_docker with context %}
{%- set sls_config_file = tplroot ~ '.config.file' %}

{%- set mountpoint = beets_docker.docker.mountpoint %}

include:
  - {{ sls_config_file }}

beets-docker-mountdir-managed:
  file.directory:
    - name: {{ mountpoint }}/docker/beets
    - makedirs: True

beets-docker-container-image-present:
  docker_image.present:
    - name: {{ beets_docker.container.image }}
    - tag: {{ beets_docker.container.tag }}
    - force: True

beets-docker-container-running:
  docker_container.running:
    - name: {{ beets_docker.container.name }}
    - image: {{ beets_docker.container.image }}:{{ beets_docker.container.tag }}
    - restart: unless-stopped
    - binds:
      - {{ mountpoint }}/docker/beets/config:/config
      - {{ mountpoint }}/shares/audio/music:/music
      - {{ mountpoint }}/shares/audio/incoming_music:/downloads
    - dns: {{ beets_docker.dns_servers }}
    - port_bindings:
      - 8337:8337
    - environment:
      - PUID: 1000
      - PGID: 100
      - TZ: Europe/London
    - watch:
      - sls: {{ sls_config_file }}
