# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as beets_docker with context %}

include:
  - {{ sls_config_clean }}

beets-docker-container-clean:
  docker_container.absent:
    - name: {{ beets_docker.container.name }}
    - force: True

beets-docker-container-image-clean:
  docker_image.absent:
    - name: {{ beets_docker.container.image }}
    - require:
      - beets-docker-container-clean
