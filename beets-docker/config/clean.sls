# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as beets_docker with context %}

{%- set mountpoint = beets_docker.docker.mountpoint %}

beets-docker-config-clean-beets-config-absent:
  file.absent:
    - name: {{ mountpoint }}/docker/beets/config/config.yaml

beets-docker-config-clean-config-directory-absent:
  file.absent:
    - name: {{ mountpoint }}/docker/beets/config
