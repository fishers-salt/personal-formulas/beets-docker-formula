# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as beets_docker with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set mountpoint = beets_docker.docker.mountpoint %}
{%- set user = beets_docker.owner.user %}
{%- set group = beets_docker.owner.group %}
{%- set apikey = beets_docker.acoustid.apikey %}

{%- for dir in [
  'docker/beets/config',
  'shares/audio/music',
  'shares/audio/incoming_music'
  ]
%}

beets-docker-config-file-{{ dir|replace('/', '_') }}-directory-managed:
  file.directory:
    - name: {{ mountpoint }}/{{ dir }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: '0755'
    - file_mode: '0644'
    - makedirs: True
    - recurse:
      - mode
      - user
      - group
{% endfor %}

beets-docker-config-file-beets-config-managed:
  file.managed:
    - name: {{ mountpoint }}/docker/beets/config/config.yaml
    - source: {{ files_switch(['config.yaml.tmpl'],
                              lookup='beets-docker-config-file-beets-config-managed',
                 )
              }}
    - user: {{ user }}
    - group: {{ group }}
    - mode: '0644'
    - makedirs: True
    - template: jinja
    - context:
        apikey: {{ apikey }}
