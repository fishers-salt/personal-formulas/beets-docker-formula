# frozen_string_literal: true

control 'beets-docker-config-clean-beets-config-absent' do
  title 'should not exist'

  describe file('/srv/docker/beets/config/config.yaml') do
    it { should_not exist }
  end
end

control 'beets-docker-config-clean-config-directory-absent' do
  title 'should not exist'

  describe directory('/srv/docker/beets/config') do
    it { should_not exist }
  end
end
