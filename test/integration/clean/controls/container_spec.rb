# frozen_string_literal: true

describe docker.containers do
  its('names') { should_not include 'beets' }
  its('images') { should_not include 'linuxserver/beets:latest' }
end
