# frozen_string_literal: true

describe docker_container(name: 'beets') do
  it { should exist }
  it { should be_running }
  its('image') { should eq 'linuxserver/beets:latest' }
  it { should have_volume('/config', '/srv/docker/beets/config') }
  it { should have_volume('/music', '/srv/shares/audio/music') }
  it { should have_volume('/downloads', '/srv/shares/audio/incoming_music') }
end
