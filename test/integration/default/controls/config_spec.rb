# frozen_string_literal: true

directories = %w[
  docker/beets/config
  shares/audio/music
  shares/audio/incoming_music
]

directories.each do |dir|
  control "beets-docker-config-file-#{dir}-managed" do
    title 'should exist'

    describe directory("/srv/#{dir}") do
      it { should exist }
      it { should be_owned_by 'kitchen' }
      it { should be_grouped_into 'users' }
      its('mode') { should cmp '0755' }
    end
  end
end

control 'beets-docker-config-file-beets-config-managed' do
  title 'should match desired lines'

  describe file('/srv/docker/beets/config/config.yaml') do
    it { should be_file }
    it { should be_owned_by 'kitchen' }
    it { should be_grouped_into 'users' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Your changes will be overwritten.') }
    its('content') { should include('    apikey: ABC123') }
  end
end
